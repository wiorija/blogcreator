<?php

namespace BlogCMS\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use BlogCMS\BlogBundle\Entity\Category;

class BlogType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', "text", array('label' => "Nom du blog"))
            ->add('description', "text", array("label" => "Description"))
            ->add('categorys', 'entity', array(
                    'class' => 'BlogCMSBlogBundle:Category',
                    'choice_label' => 'getName',
                    'empty_value' => null,
                    'label' => "Catégorie",
                    'query_builder' => function(\BlogCMS\BlogBundle\Entity\CategoryRepository $er) use ($options) {
                        return  $er->createQueryBuilder('a')
                                ->where('a.active = :active')            
                                ->andWhere('a.user = :id')            
                                ->setParameter('active', 1)
                                ->setParameter('id', $options['userId']);
                    }   
            ));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BlogCMS\BlogBundle\Entity\Blog',
            'userId' => ""
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'blogcms_blogbundle_blog';
    }
}
