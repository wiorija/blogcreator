<?php

namespace BlogCMS\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use BlogCMS\BlogBundle\Entity\BlogRepository;
use BlogCMS\UserBundle\Entity\User;

class ArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder
            ->add('title')
            ->add('description')
            ->add('body')
            ->add('attachedFile', 'file', array(
                    'data_class' => null,
                    'required' => false,
            ))
            ->add('blog', 'entity', array(
                    'class' => 'BlogCMSBlogBundle:Blog',
                    'choice_label' => 'getName',
                    'query_builder' => function(\BlogCMS\BlogBundle\Entity\BlogRepository $er) use ($options) {
                        return  $er->createQueryBuilder('a')
                                ->where('a.user = :id')             
                                ->setParameter('id', $options['userId']);
                    }   
            ));
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BlogCMS\BlogBundle\Entity\Article',
            'userId' => ""
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'blogcms_blogbundle_article';
    }
}
