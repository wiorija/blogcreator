<?php

namespace BlogCMS\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Blog
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BlogCMS\BlogBundle\Entity\BlogRepository")
 * @JMS\ExclusionPolicy("NONE")
 */
class Blog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Merci de renseigner un nom")
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Merci de renseigner une description")
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="BlogCMS\BlogBundle\Entity\Article", mappedBy="blog", cascade={"persist"})
     */
    private $articles;

    /**
     * @var integer
     * @Assert\NotBlank(message="Merci de renseigner une catégorie")
     * @ORM\ManyToOne(targetEntity="BlogCMS\BlogBundle\Entity\Category", cascade={"persist"})
     */
    private $categorys;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="BlogCMS\UserBundle\Entity\User", cascade={"persist"})
     */
    private $user;

    /**
     * @var integer
     * @Assert\NotBlank(message="Merci de remplir le champ active")
     * @ORM\Column(name="active", type="integer")
     */
    private $active = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     * @var string
     *
     * @ORM\ManyToMany(targetEntity="BlogCMS\UserBundle\Entity\User", mappedBy="blogFavoris", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $followers;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categorys = new \Doctrine\Common\Collections\ArrayCollection();
        $this->updatedAt = new \DateTime("now");
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Blog
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return Blog
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Blog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Blog
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add article
     *
     * @param \BlogCMS\BlogBundle\Entity\Article $article
     *
     * @return Blog
     */
    public function addArticle(\BlogCMS\BlogBundle\Entity\Article $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \BlogCMS\BlogBundle\Entity\Article $article
     */
    public function removeArticle(\BlogCMS\BlogBundle\Entity\Article $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add category
     *
     * @param \BlogCMS\BlogBundle\Entity\Category $category
     *
     * @return Blog
     */
    public function addCategory(\BlogCMS\BlogBundle\Entity\Category $category)
    {
        $this->categorys[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \BlogCMS\BlogBundle\Entity\Category $category
     */
    public function removeCategory(\BlogCMS\BlogBundle\Entity\Category $category)
    {
        $this->categorys->removeElement($category);
    }

    /**
     * Get categorys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorys()
    {
        return $this->categorys;
    }

    /**
     * Set categorys
     *
     * @param \BlogCMS\BlogBundle\Entity\Category $categorys
     *
     * @return Blog
     */
    public function setCategorys(\BlogCMS\BlogBundle\Entity\Category $categorys = null)
    {
        $this->categorys = $categorys;

        return $this;
    }

    /**
     * Set user
     *
     * @param \BlogCMS\UserBundle\Entity\User $user
     *
     * @return Blog
     */
    public function setUser(\BlogCMS\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BlogCMS\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add follower
     *
     * @param \BlogCMS\UserBundle\Entity\User $follower
     *
     * @return Blog
     */
    public function addFollower(\BlogCMS\UserBundle\Entity\User $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }
    
    /** Set description
     *
     * @param string $description
     *
     * @return Blog
     */
    public function setDescription($description)
    {
        $this->description = $description;
        
        return $this;
    }

    /**
     * Remove follower
     *
     * @param \BlogCMS\UserBundle\Entity\User $follower
     */
    public function removeFollower(\BlogCMS\UserBundle\Entity\User $follower)
    {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers()
    {
        return $this->followers;
    }
    
    /** Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
