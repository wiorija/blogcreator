<?php

namespace BlogCMS\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BlogCMS\BlogBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Merci de renseigner un nom")
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="BlogCMS\BlogBundle\Entity\Blog", mappedBy="categorys", cascade={"persist"})
     */
    private $blog;
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="BlogCMS\UserBundle\Entity\User", cascade={"persist"})
     */
    private $user;

    /**
     * @var integer
     * @Assert\NotBlank(message="Merci de remplir le champ active")
     * @ORM\Column(name="active", type="integer")
     */
    private $active = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return Category
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Category
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Category
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set category
     *
     * @param \BlogCMS\BlogBundle\Entity\Blog $category
     *
     * @return Category
     */
    public function setCategory(\BlogCMS\BlogBundle\Entity\Blog $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \BlogCMS\BlogBundle\Entity\Blog
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set blog
     *
     * @param \BlogCMS\BlogBundle\Entity\Blog $blog
     *
     * @return Category
     */
    public function setBlog(\BlogCMS\BlogBundle\Entity\Blog $blog = null)
    {
        $this->blog = $blog;

        return $this;
    }

    /**
     * Get blog
     *
     * @return \BlogCMS\BlogBundle\Entity\Blog
     */
    public function getBlog()
    {
        return $this->blog;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->blog = new \Doctrine\Common\Collections\ArrayCollection();
        $this->updatedAt = new \DateTime("now");
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Add blog
     *
     * @param \BlogCMS\BlogBundle\Entity\Blog $blog
     *
     * @return Category
     */
    public function addBlog(\BlogCMS\BlogBundle\Entity\Blog $blog)
    {
        $this->blog[] = $blog;

        return $this;
    }

    /**
     * Remove blog
     *
     * @param \BlogCMS\BlogBundle\Entity\Blog $blog
     */
    public function removeBlog(\BlogCMS\BlogBundle\Entity\Blog $blog)
    {
        $this->blog->removeElement($blog);
    }

    /**
     * Set user
     *
     * @param \BlogCMS\UserBundle\Entity\User $user
     *
     * @return Category
     */
    public function setUser(\BlogCMS\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BlogCMS\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
