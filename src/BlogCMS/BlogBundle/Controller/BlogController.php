<?php

namespace BlogCMS\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use BlogCMS\BlogBundle\Entity\Blog;
use BlogCMS\UserBundle\Entity\User;
use BlogCMS\BlogBundle\Entity\Category;
use BlogCMS\BlogBundle\Entity\Article;
use BlogCMS\BlogBundle\Form\BlogType;

/**
 * Blog controller.
 *
 */
class BlogController extends Controller
{

    /**
     * Lists all Blog entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BlogCMSBlogBundle:Blog')->findBy(array('active' => 1));

        return $this->render('BlogCMSBlogBundle:Blog:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function myIndexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if (!$this->getUser()) {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $entities = $em->getRepository('BlogCMSBlogBundle:Blog')->findBy(array('active' => 1, 'user' => $this->getUser()->getId()));

        return $this->render('BlogCMSBlogBundle:Blog:myblog.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Blog entity.
     *
     */
    public function createAction(Request $request)
    {
        $session = $this->get('session');
        $entity = new Blog();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($this->getUser());
            $em->persist($entity);
            try {
                $em->flush();
            } catch (\Exception $ex) {
                $session->getFlashBag()->add('notice', 'Ce blog existe déjà');
            }

            return $this->redirect($this->generateUrl('blog', array('id' => $entity->getId())));
        }

        return $this->render('BlogCMSBlogBundle:Blog:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Blog entity.
     *
     * @param Blog $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Blog $entity)
    {
        $form = $this->createForm(new BlogType(), $entity, array(
            'userId' => $this->getUser()->getId(),
            'action' => $this->generateUrl('blog_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Blog entity.
     *
     */
    public function newAction()
    {
        $entity = new Blog();
        $form = $this->createCreateForm($entity);

        return $this->render('BlogCMSBlogBundle:Blog:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Blog entity.
     *
     */
    public function showAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('BlogCMSBlogBundle:Blog')->findOneBy(array('id' => $id, 'active' => 1));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Blog entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BlogCMSBlogBundle:Blog:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Blog entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BlogCMSBlogBundle:Blog')->findOneBy(array('id' => $id, 'user' => $this->getUser()->getId()));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Blog entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BlogCMSBlogBundle:Blog:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Blog entity.
    *
    * @param Blog $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Blog $entity)
    {
        $form = $this->createForm(new BlogType(), $entity, array(
            'userId' => $this->getUser()->getId(),
            'action' => $this->generateUrl('blog_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Blog entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $session = $this->get('session');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BlogCMSBlogBundle:Blog')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Blog entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            try {
                $em->flush();
            } catch (\Exception $ex) {
                $session->getFlashBag()->add('notice', 'Ce blog existe déjà');
            }
            return $this->redirect($this->generateUrl('blog'));
        }

        return $this->render('BlogCMSBlogBundle:Blog:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Blog entity.
     *
     */
    public function deleteAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BlogCMSBlogBundle:Blog')->findOneBy(array('id' => $id, 'user' => $this->getUser()->getId()));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Blog entity.');
        }

        foreach ($entity->getArticles() as $value) {
            $value->setActive(0);
        }
        $entity->setActive(0);
        $em->flush();

        return $this->redirect($this->generateUrl('blog'));
    }

    /**
     * Creates a form to delete a Blog entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blog_delete', array('id' => $id)))
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    /**
     * @ParamConverter("blog", class="BlogCMSBlogBundle:Blog")
     * 
     */
    public function followBlogAction(Blog $blog) {
        $session = $this->get('session');
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getUser();
        $user->addBlogFavori($blog);

        try {
            $em->flush();
            $session->getFlashBag()->add('notice', 'Vous suivez desormais le blog ' . $blog->getName());
        } catch (\Exception $ex) {
            $session->getFlashBag()->add('notice', 'Vous suivez deja le blog ' . $blog->getName());
        }
        
        return $this->redirect($this->generateUrl('blog_list'));
        
    }

    /**
     * @ParamConverter("user", class="UserBundle:User")
     * 
     */
    public function contactAction(User $user, Request $request) {
        $session = $this->get('session');
        $form = $this->createFormBuilder(null, array('csrf_protection' => false))
            ->add('sujet', 'text', array('required' => true))
            ->add('contenu', 'textarea',  array('required' => true, 'attr' => array('class' => 'textareaContact')))
            ->add('submit', 'submit', array('label' => 'Envoyer'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $message = \Swift_Message::newInstance()
                ->setSubject($request->request->get('form')['sujet'])
                ->setFrom($this->getUser()->getEmail())
                ->setTo($user->getEmail())
                ->setBody($request->request->get('form')['contenu']);
            $this->get('mailer')->send($message);

            $session->getFlashBag()->add('notice', 'Votre email à été envoyé.');
            return $this->redirect($this->generateUrl('blog'));
        }

        return $this->render('BlogCMSBlogBundle:Blog:contact.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    /**
     * @ParamConverter("blog", class="BlogCMSBlogBundle:Blog")
     * 
     */
    public function unFollowBlogAction(Blog $blog) {
        $session = $this->get('session');
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getUser();
        $user->removeBlogFavori($blog);

        try {
            $em->flush();
            $session->getFlashBag()->add('notice', 'Vous ne suivez plus le blog ' . $blog->getName());
        } catch (\Exception $ex) {
//            $session->getFlashBag()->add('notice', 'Vous suiviez desormais le blog ' . $blog->getName());
        }
        
        
        return $this->redirect($this->generateUrl('blog_favoris'));
        
    }
    
    public function favorisAction(Request $request) {
        $user = $this->getUser();
        
        $blogs = $user->getBlogFavoris();
        
        $paginator  = $this->get('knp_paginator');
        
        $pagination = $paginator->paginate(
            $blogs,
            $request->query->getInt('page', 1),
            5
        );
        return $this->render('BlogCMSBlogBundle:Blog:favoris.html.twig', array('pagination' => $pagination));
         
    }
    
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        
        $blogs = $em->getRepository('BlogCMSBlogBundle:Blog')->getAllBlogsExceptCurrentsUser($user);
        
        $paginator  = $this->get('knp_paginator');
        
        $pagination = $paginator->paginate(
            $blogs,
            $request->query->getInt('page', 1),
            5
        );
        return $this->render('BlogCMSBlogBundle:Blog:list.html.twig', array('pagination' => $pagination));
    }
}
