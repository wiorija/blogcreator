<?php

namespace BlogCMS\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        
        $blogs = $em->getRepository('BlogCMSBlogBundle:Blog')->getAllBlogsExceptCurrentsUser($user);
        
        $paginator  = $this->get('knp_paginator');
        
        $pagination = $paginator->paginate(
            $blogs,
            $request->query->getInt('page', 1),
            5
        );
        return $this->render('BlogCMSBlogBundle:Default:index.html.twig', array('pagination' => $pagination));
    }
}
