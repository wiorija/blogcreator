<?php

namespace BlogCMS\BlogBundle\Twig;

class FileExtension extends \Twig_Extension {
    private $assetic_path;
    
    public function __construct($assetic) {
        $this->assetic_path = $assetic;
    }
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('file', array($this, 'fileFilter')),
        );
    }

    public function fileFilter($article, $type = false)
    {
        $file = 'images/article/' . $article->getId() . '/' . $article->getAttachedFile();
        
        if ($type == true) {
            if (preg_match('/jpg/i', $file) == 1 || preg_match('/png/i', $file) == 1 || preg_match('/jpeg/i', $file) == 1) {
                return true;
            } else {
                return false;
            }
        }
        if (file_exists($this->assetic_path . '/' . $file) && (preg_match('/jpg/', $file) == 1 || preg_match('/png/', $file) == 1 || preg_match('/jpeg/', $file) == 1)) {
            return $file;
        } else {
            return 'public/img/article.jpg';
        }
    }
    
    public function getName()
    {
        return 'file_extension';
    }
}

