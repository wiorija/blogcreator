<?php

namespace BlogCMS\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use BlogCMS\UserBundle\Entity\User;

class UserController extends Controller
{
    /**
     * @ParamConverter("user", class="UserBundle:User")
     * 
     */
    public function getUserAction(User $user)
    {
        return $this->render('UserBundle:User:getUser.html.twig', array('user' => $user));    
       
    }

}
