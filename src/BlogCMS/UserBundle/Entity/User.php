<?php

namespace BlogCMS\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="BlogCMS\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="BlogCMS\BlogBundle\Entity\Blog", mappedBy="user")
     */
    private $blog;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="BlogCMS\BlogBundle\Entity\Comment", mappedBy="user")
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="BlogCMS\BlogBundle\Entity\Category", mappedBy="user")
     */
    private $category;
    
     /**
     * @var string
     *
     * @ORM\ManyToMany(targetEntity="BlogCMS\BlogBundle\Entity\Blog", inversedBy="followers", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $blogFavoris;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __construct() {
        parent::__construct();
        
        $this->roles = array('ROLE_USER');
    }

    /**
     * Add blog
     *
     * @param \BlogCMS\BlogBundle\Entity\Blog $blog
     *
     * @return User
     */
    public function addBlog(\BlogCMS\BlogBundle\Entity\Blog $blog)
    {
        $this->blog[] = $blog;

        return $this;
    }

    /**
     * Remove blog
     *
     * @param \BlogCMS\BlogBundle\Entity\Blog $blog
     */
    public function removeBlog(\BlogCMS\BlogBundle\Entity\Blog $blog)
    {
        $this->blog->removeElement($blog);
    }

    /**
     * Get blog
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * Add comment
     *
     * @param \BlogCMS\BlogBundle\Entity\Comment $comment
     *
     * @return User
     */
    public function addComment(\BlogCMS\BlogBundle\Entity\Comment $comment)
    {
        $this->comment[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \BlogCMS\BlogBundle\Entity\Comment $comment
     */
    public function removeComment(\BlogCMS\BlogBundle\Entity\Comment $comment)
    {
        $this->comment->removeElement($comment);
    }

    /**
     * Get comment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add category
     *
     * @param \BlogCMS\BlogBundle\Entity\Category $category
     *
     * @return User
     */
    public function addCategory(\BlogCMS\BlogBundle\Entity\Category $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \BlogCMS\BlogBundle\Entity\Category $category
     */
    public function removeCategory(\BlogCMS\BlogBundle\Entity\Category $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add blogFavori
     *
     * @param \BlogCMS\BlogBundle\Entity\Blog $blogFavori
     *
     * @return User
     */
    public function addBlogFavori(\BlogCMS\BlogBundle\Entity\Blog $blogFavori)
    {
        $this->blogFavoris[] = $blogFavori;

        return $this;
    }

    /**
     * Remove blogFavori
     *
     * @param \BlogCMS\BlogBundle\Entity\Blog $blogFavori
     */
    public function removeBlogFavori(\BlogCMS\BlogBundle\Entity\Blog $blogFavori)
    {
        $this->blogFavoris->removeElement($blogFavori);
    }

    /**
     * Get blogFavoris
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlogFavoris()
    {
        return $this->blogFavoris;
    }
}
