créer un fichier dans /etc/apache2/sites-available/blog.conf
<VirtualHost blog-creator.prod:80>
    ServerName blog-creator.prod
    ServerAlias blog-creator.prod
    ServerAdmin  blog-creator.prod
    DocumentRoot /var/www/blogCreator/web/
    <Directory var/www/blogCreator/web/>
        Options Indexes FollowSymLinks MultiViews
        Require all granted
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>

sudo a2ensite blog.conf
ajouter la ligne 127.0.0.1 blog-creator.prod
sudo service apache2 restart

php app/console cache:clear --env=prod